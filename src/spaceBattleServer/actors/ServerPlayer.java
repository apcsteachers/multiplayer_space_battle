package spaceBattleServer.actors;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.imageio.ImageIO;

import gameServer.Collision;
import gameServer.CollisionMap;
import gameServer.MultiThreadServer;
import gameServer.MultiplayerActorModel;
import gameServer.WorldModel;
import javafx.geometry.Point2D;
import spaceBattleClient.actors.ClientPlayer;

public class ServerPlayer extends MultiplayerActorModel {
	private double dx;
	private double dy;
	private double maxSpeed;
	private int fireDelay;
	private int fireCooldown;

	public ServerPlayer(String actorId) throws IOException {
		super(actorId);
		InputStream stream = CollisionMap.class.getClassLoader().getResourceAsStream("playerShip2_red.png");
		BufferedImage img = ImageIO.read(stream);
		setImage(img);
		dx = Math.random() * 3 - 1;
		dy = Math.random() * 3 - 1;
		maxSpeed = 15;
		fireDelay = 20;
		fireCooldown = 0;
		setClientClass(ClientPlayer.class);
	}
	
	public void onHitByLaser() {
		getWorld().remove(this);
	}
	
	public void attemptToFire() {
		if (fireCooldown == 0) {
			fireCooldown = fireDelay;
			double x = getCenterX();
			double y = getCenterY();
			try {
				ServerLaser laser = new ServerLaser(MultiThreadServer.generateUUID(), (int)(30 * getScaleX()), 10);
				laser.setZOrder(getZOrder() - 1);
				laser.setX(x - laser.getWidth() / 2);
				laser.setY(y - laser.getHeight() / 2);
				laser.setRotation(getRotation());
				laser.setScale(getScaleX(), getScaleY());
				laser.setOwnerId(getActorId());
				getWorld().add(laser);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void act() {
		if (fireCooldown > 0) fireCooldown--;
		move(dx, dy);
		edgeLoop();
		handleCollisionWithPlayers();
	}
	
	private void edgeLoop() {
		WorldModel world = getWorld();
		if (getX() > world.getWidth() - getWidth() / 2) {
			setX(-getWidth() / 2);
		} else if (getX() < -getWidth() / 2) {
			setX(world.getWidth() - getWidth() / 2);
		}
		
		if (getY() > world.getHeight() - getHeight() / 2) {
			setY(-getHeight() / 2);
		} else if (getY() < -getHeight() / 2) {
			setY(world.getHeight() - getHeight() / 2);
		}
	}
	
	public void handleCollisionWithPlayers() {
		List<ServerPlayer> touchedPlayers = getIntersectingActors(ServerPlayer.class);
		double cX = getCenterX();
		double cY = getCenterY();
		for (ServerPlayer a : touchedPlayers) {
			Collision collision = new Collision(10, new Point2D(dx, dy), new Point2D(cX, cY), 10, new Point2D(a.getDx(), a.getDy()), new Point2D(a.getX() + a.getWidth() / 2, a.getY() + a.getHeight() / 2));
			setDx(collision.getResultV1().getX());
			setDy(collision.getResultV1().getY());
			a.setDx(collision.getResultV2().getX());
			a.setDy(collision.getResultV2().getY());
		}
	}
	
	public void accelerate(double amount) {
		double ddx = amount * Math.cos(getRotation());
		double ddy = amount * Math.sin(getRotation());
		double dx = getDx() + ddx;
		double dy = getDy() + ddy;
		double speedSqrd = dx * dx + dy * dy;
		if (speedSqrd <= maxSpeed * maxSpeed) {
			setDx(dx);
			setDy(dy);
		}
	}

	public double getDx() {
		return dx;
	}

	public void setDx(double dx) {
		this.dx = dx;
	}

	public double getDy() {
		return dy;
	}

	public void setDy(double dy) {
		this.dy = dy;
	}
	
	

}
