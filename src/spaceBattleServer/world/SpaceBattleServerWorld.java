package spaceBattleServer.world;

import java.io.IOException;
import java.net.URISyntaxException;

import gameServer.MultiThreadServer;
import gameServer.MultiplayerActorModel;
import gameServer.ServerWorld;
import spaceBattleServer.actors.ServerPlayer;

public class SpaceBattleServerWorld extends ServerWorld {

	public SpaceBattleServerWorld(double width, double height, long delay, MultiThreadServer server) {
		super(width, height, delay, server);
	}

	public SpaceBattleServerWorld(double width, double height, MultiThreadServer server) {
		super(width, height, server);
	}

	public void addPlayer(String id) throws URISyntaxException, IOException {
		addPlayer(id, Math.random() * getWidth(), Math.random() * getHeight());
	}

	public void addPlayer(String id, double x, double y) throws URISyntaxException, IOException {
		if (!getMultiplayerActors().containsKey(id)) {
			MultiplayerActorModel p = new ServerPlayer(id);
			p.setX(x);
			p.setY(y);
			add(p);
		} else {
			System.err.println("Add player failed because player with id " + id + " already exists");
		}
	}

	@Override
	public void act() {
		

	}

	@Override
	public void onClientConnect(String id) {
		try {
			addPlayer(id);
		} catch (URISyntaxException | IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClientDisconnect(String id) {
		removeActor(id);

	}
}
