package spaceBattleClient;

import gameClient.GameClient;
import graphicsEngine.World;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.converter.IntegerStringConverter;
import spaceBattleClient.world.ClientWorld;

public class ClientDriver extends Application {

	private Button joinButton;
	private TextField ipAddressTextField;
	private TextField portTextField;
	private ClientWorld world;
	private HBox bottomHBox;
	private Text connectingText;
	private BorderPane root;
	
	private ImageView title;
	private Scene scene;

	@Override
	public void start(Stage stage) throws Exception {
		root = new BorderPane();
		bottomHBox = new HBox();
		bottomHBox.setSpacing(10);
		bottomHBox.setAlignment(Pos.CENTER);
		bottomHBox.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
		bottomHBox.setPadding(new Insets(10));
		root.setBottom(bottomHBox);
		Text ipAddressLabel = new Text("IP ADRESS");
		bottomHBox.getChildren().add(ipAddressLabel);
		ipAddressTextField = new TextField();
		ipAddressTextField.setMaxWidth(100);
		ipAddressTextField.setText("localhost");

		Text portLabel = new Text("PORT");
		bottomHBox.getChildren().add(portLabel);
		portTextField = new TextField();
		portTextField.setTextFormatter(new TextFormatter<Integer>(new IntegerStringConverter()));
		portTextField.setMaxWidth(50);
		portTextField.setText("1234");
		bottomHBox.getChildren().add(portTextField);

		root.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

		connectingText = new Text("Connecting...");
		connectingText.setFont(new Font(36));
		connectingText.setStroke(Color.WHITE);
		connectingText.setTextAlignment(TextAlignment.CENTER);
		
		title = new ImageView(new Image(getClass().getClassLoader().getResource("SpaceWarsTitle.png").toString()));
		

		joinButton = new Button("Join");
		joinButton.setOnAction(e -> {
			try {
				String hostName = ipAddressTextField.getText().trim();
				int portNumber = Integer.parseInt(portTextField.getText());
				layoutConnectingUI();
				GameClient client = new GameClient(hostName, portNumber);
				ChangeListener<Boolean> disconnectListener = new ChangeListener<Boolean>() {

					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
							Boolean newValue) {
						if (!newValue.booleanValue()) {
							Platform.runLater(() -> {
								layoutJoinUI();
								title.requestFocus();
							});
						} else {
							Platform.runLater(() -> {
								layoutPlayingUI(world);
								world.requestFocus();
							});
						}
					}
				};
				client.getConnected().addListener(disconnectListener);
				Thread clientThread = new Thread(client);
				clientThread.start();
				if (world != null) world.stop();
				world = new ClientWorld(client);
				client.setWorld(world);
				world.start();
				Thread timeOutThread = new Thread(() -> {
					try {
						Platform.runLater(()-> {
							connectingText.setText("Connecting...");
						});
						for (int i = 10; i >= 0; i--) {
							Thread.sleep(1000);
							Platform.runLater(()-> {
								connectingText.setText(connectingText.getText() + ".");
							});
						}
						if (!client.getConnected().getValue()) {
							client.getConnected().removeListener(disconnectListener);
							clientThread.interrupt();
							Platform.runLater(() -> {
								layoutJoinUI();
								world.requestFocus();
							});
							System.out.println("CONNECTION FAILED");
						} else {
							System.out.println("CONNECTION SUCCEEDED");
						}
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				});

				timeOutThread.start();
			} catch (Throwable err) {
				err.printStackTrace();
			}
		});
		scene = new Scene(root, 1000, 700);
		layoutJoinUI();
		stage.setScene(scene);
		stage.show();
	}

	public void layoutJoinUI() {
		bottomHBox.getChildren().removeAll(bottomHBox.getChildren());
		bottomHBox.getChildren().add(ipAddressTextField);
		bottomHBox.getChildren().add(portTextField);
		bottomHBox.getChildren().add(joinButton);
		title.setFitWidth(scene.getWidth());
		root.setCenter(title);
	}

	public void layoutConnectingUI() {
		bottomHBox.getChildren().removeAll(bottomHBox.getChildren());
		connectingText.setText("Connecting...");
		bottomHBox.getChildren().add(connectingText);
	}

	public void layoutPlayingUI(World world) {
		bottomHBox.getChildren().removeAll(bottomHBox.getChildren());
		root.setCenter(world);
	}

	public void setVisibilityOfChildren(Region reg, boolean visible) {
		for (Node child : reg.getChildrenUnmodifiable()) {
			child.setVisible(visible);
		}
	}

	@Override
	public void stop() throws Exception {
		super.stop();
		System.exit(0);
	}

	public static void main(String[] args) {
		launch();
	}

}
