package spaceBattleClient.effects;
import graphicsEngine.World;

public interface Effect {
	public void addToWorld(World world);
	public void run(World world, double x, double y);
}
