package spaceBattleClient.effects;

import java.util.Random;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;
import spaceBattleClient.actors.Particle;

public class PixelBurst extends PreloadedParticleEffect {
	
	public PixelBurst(Image img, double minAngle, double maxAngle, double minSpeed, double maxSpeed, double dOpacity) {
		double w = img.getWidth();
        double h = img.getHeight();
        
        Random rand = new Random();
        for (int r = 0; r < h; r += 4) {
        	for (int c = 0; c < w; c += 4) {
        		double angle = new Random().nextDouble() * (maxAngle - minAngle) + minAngle;
        		double angleRad = Math.toRadians(90 - angle);
                PixelReader pixR = img.getPixelReader();
                double speed =  rand.nextDouble() * (maxSpeed - minSpeed) + minSpeed;
                Color color = pixR.getColor(c, r);
                Particle p = new Particle();
                p.setFitWidth(4);
                p.setFitHeight(4);
                p.fillWithColor(color);
                p.setdOpacity(dOpacity);
                p.setDx((int)(speed * Math.cos(angleRad)));
                p.setDy((int)(speed * Math.sin(angleRad)));
                p.setX(c);
                p.setY(r);
                p.setTimeToLive(80);
                getParticles().add(p);
        	}
        }
	}


}
