package spaceBattleClient.effects;

import java.util.ArrayList;
import java.util.List;

import graphicsEngine.World;
import spaceBattleClient.actors.Particle;

public abstract class PreloadedParticleEffect implements Effect {

	private boolean hasRun;
	private List<Particle> particles;

	public PreloadedParticleEffect() {
		particles = new ArrayList<>();
		hasRun = false;
	}

	public PreloadedParticleEffect(List<Particle> particles) {
		particles.addAll(particles);
	}
	
	@Override
	public void addToWorld(World world) {
		for (Particle p : particles) {
			p.setVisible(false);
			p.setActive(false);
			world.add(p);
		}
	}
	
	@Override
	public void run(World world, double x, double y) {
		if (!hasRun) {
			hasRun = true;
			for (Particle p : particles) {
				p.setX(p.getX() + x);
				p.setY(p.getY() + y);
				p.setVisible(true);
				p.setActive(true);
			}
		}
	}

	public List<Particle> getParticles() {
		return particles;
	}

	public void setParticles(List<Particle> particles) {
		this.particles = particles;
	}

	public boolean isHasRun() {
		return hasRun;
	}
	
	
}
