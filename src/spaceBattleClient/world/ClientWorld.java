package spaceBattleClient.world;

import java.lang.reflect.InvocationTargetException;

import gameClient.GameClient;
import gameClient.MultiplayerActor;
import gameClient.MultiplayerWorld;
import graphicsEngine.Sound;
import javafx.geometry.Insets;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import spaceBattleClient.actors.ClientPlayer;
import spaceBattleClient.actors.GameOver;

public class ClientWorld extends MultiplayerWorld {
	
	private boolean playerIsAlive;

	public ClientWorld(GameClient client) {
		super(client);
		setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
	}

	@Override
	public void act(long arg0) {
		
	}

	@Override
	public void remove(MultiplayerActor a) {
		String id = a.getActorId();
		super.remove(a);
		if (getClient().getId().equals(id)) {
			Sound snd = new Sound("youHaveBeenAstroid.wav");
			snd.play();
			System.out.println("You have been destroyed!");
			remove(getObjects());
			GameOver go = new GameOver();
			go.setPreserveRatio(true);
			go.setFitWidth(getWidth());
			add(go);
			go.setY(getHeight() / 2 - go.getHeight() / 2);
			getClient().setStayConnected(false);
		}
	}

	@Override
	public ClientPlayer createPlayer(String id) {
		try {
			return (ClientPlayer) createActor(ClientPlayer.class, id);
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean isPlayerIsAlive() {
		return playerIsAlive;
	}

	public void setPlayerIsAlive(boolean playerIsAlive) {
		this.playerIsAlive = playerIsAlive;
	}

	
}
