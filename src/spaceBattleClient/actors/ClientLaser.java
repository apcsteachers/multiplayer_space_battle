package spaceBattleClient.actors;
import gameClient.MultiplayerActor;
import graphicsEngine.Sound;

public class ClientLaser extends MultiplayerActor {
	
	private static Sound fireSnd = new Sound("laser-wrath-4.wav");
	
	public ClientLaser(String actorId) {
		super(actorId);
		setImage("laserGreen04.png");
	}
	
	@Override
	public void addedToWorld() {
		super.addedToWorld();
		fireSnd.play();
	}

	@Override
	public void act(long now) {
		
	}
}
