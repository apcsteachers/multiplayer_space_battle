package spaceBattleClient.actors;
import java.util.ArrayList;
import java.util.List;

import gameClient.GameClient;
import gameClient.MultiplayerActor;
import gameClient.MultiplayerWorld;
import graphicsEngine.Sound;
import graphicsEngine.World;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import spaceBattleClient.effects.Effect;
import spaceBattleClient.effects.PixelBurst;
import spaceBattleServer.commands.AccelerateCommand;
import spaceBattleServer.commands.EnlargeCommand;
import spaceBattleServer.commands.FireCommand;
import spaceBattleServer.commands.ReduceCommand;
import spaceBattleServer.commands.RotateLeftCommand;
import spaceBattleServer.commands.RotateRightCommand;

public class ClientPlayer extends MultiplayerActor {
	
	private Rectangle boundsRect;
	private Rectangle centerRect;
	private boolean showRectangles;
	private final long commandDelay;
	private long commandCooldown;
	private long then;
	private List<Effect> effectsOnDeath;
	
	public ClientPlayer(String actorId) {
		super(actorId);
		setImage("playerShip2_red.png");
		
		boundsRect = new Rectangle(getWidth(), getHeight());
		boundsRect.setFill(new Color(1.0,0,0,0.2));
		
		centerRect = new Rectangle(10, 10);
		centerRect.setFill(new Color(0,1.0,1.0,0.4));
		
		this.showRectangles = false;
		
		setOnKeyReleased(e-> {
			if (e.getCode() == KeyCode.H) {
				toggleRects();
			} else if (e.getCode() == KeyCode.ESCAPE) {
				if (getWorld() != null) getWorld().remove(this);
			}
		});
		
		effectsOnDeath = new ArrayList<>();
		effectsOnDeath.add(new PixelBurst(this.getImage(), 0, 360, 1, 10, 0.01));
		
		commandDelay = 16_000_000;
		commandCooldown = 0;
		then = 0;
	}
	
	@Override
	public void addedToWorld() {
		super.addedToWorld();
		getWorld().getChildren().add(boundsRect);
		getWorld().getChildren().add(centerRect);
		setRectVisibility(showRectangles);
		for (Effect ef : effectsOnDeath) {
			ef.addToWorld(getWorld());
		}
	}
	
	@Override
	public void removedFromWorld(World world) {
		world.getChildren().remove(boundsRect);
		world.getChildren().remove(centerRect);
		for (Effect ef : effectsOnDeath) {
			ef.run(world, getX(), getY());
		}
		new Sound("atari_boom.wav").play();
	}
	
	private void toggleRects() {
		showRectangles = !showRectangles;
		setRectVisibility(showRectangles);
		if (showRectangles) {
			updateRects();
		}
	}
	
	private void setRectVisibility(boolean visible) {
		centerRect.setVisible(visible);
		boundsRect.setVisible(visible);
	}
	
	private void updateRects() {
		boundsRect.setWidth(getBoundsInParent().getWidth());
		boundsRect.setHeight(getBoundsInParent().getHeight());
		boundsRect.setX(getBoundsInParent().getMinX());
		boundsRect.setY(getBoundsInParent().getMinY());
		
		centerRect.setX(getCenterX() - centerRect.getWidth() / 2);
		centerRect.setY(getCenterY() - centerRect.getHeight() / 2);
	}

	@Override
	public void act(long now) {
		long dt = now - then;
		then = now;
		MultiplayerWorld world = (MultiplayerWorld) getWorld();
		if (showRectangles) {
			updateRects();
		}
		
		GameClient client = world.getClient();
		commandCooldown = Math.max(0, commandCooldown - dt);
		if (commandCooldown == 0) {
			commandCooldown = commandDelay;
			if (world.isKeyPressed(KeyCode.W) || world.isKeyPressed(KeyCode.UP)) {
				client.sendMessage(new AccelerateCommand().getCommandWord());
			}
			
			if (getWorld().isKeyPressed(KeyCode.X)) {
				client.sendMessage(new EnlargeCommand().getCommandWord());
			} else if (getWorld().isKeyPressed(KeyCode.Z)) {
				client.sendMessage(new ReduceCommand().getCommandWord());
			}
	
			if (getWorld().isKeyPressed(KeyCode.D) || world.isKeyPressed(KeyCode.RIGHT)) {
				client.sendMessage(new RotateRightCommand().getCommandWord());
			} else if (getWorld().isKeyPressed(KeyCode.A) || world.isKeyPressed(KeyCode.LEFT)) {
				client.sendMessage(new RotateLeftCommand().getCommandWord());
			}
			
			if (getWorld().isKeyPressed(KeyCode.SPACE)) {
				client.sendMessage(new FireCommand().getCommandWord());
			}
		}
	}
}
